FROM scratch

LABEL maintaner="Ruslan Yeraliyev <ruslan.yeraliyev@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
